provider "aws" {
  region = "eu-central-1"
}


resource "aws_s3_bucket" "curly_bucket01" {
  bucket = "curly.bucket01"
  acl    = "private"

  tags = {
    Name        = "My bucket02"
    Environment = "Test"
  }
}

resource "aws_s3_bucket_public_access_block" "b" {
  bucket              = aws_s3_bucket.curly_bucket01.id
  block_public_acls   = true
  block_public_policy = true
}

resource "aws_s3_account_public_access_block" "b" {
  block_public_acls   = true
  block_public_policy = true
}



